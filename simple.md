# Simple builds

> Simple builds are deprecated due to bad support and low adoption. Starting from Gugal 0.9 (upcoming), simple builds will no longer be available.

Starting from 0.7 Gugal has a new simple build type. These builds remove the navigation bar and News section, leaving only the search features.

These builds aren't officially supported yet and might have some issues. Simple builds of stable versions will be released alongside full builds on GitLab, but F-Droid releases will be full builds.

To test a simple build you can download a simple build of 0.7 preview 1 [here](https://gitlab.com/gugal/beta/-/raw/main/apk/0.7/Gugal-0.7.p1-simple.apk?inline=false). It can be installed alongside regular Gugal versions, but ones built from source can't.

## Building
To build a simple build, simply use either the `simpleDebug` or `simpleRelease` build variants for a debug or release build respectively.