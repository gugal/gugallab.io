# Preview versions of Gugal

Do you want to use upcoming versions of Gugal early and help shape the future of the app? Then you can beta test it by installing preview versions. These versions are less stable than release ones but have more features.

> Preview versions are incompatible with the F-Droid version.

**Once you install a preview version you will only be able to leave testing without losing data when the version becomes stable.** For example, if updating from Gugal 0.4.1 to 0.5 preview, you will be able to return to stable versions without losing data once 0.5 is released. You can also delete Gugal and install 0.4.1, losing any saved data.

Also, currently preview versions don't update automatically, so check the preview page from time to time.

Interested? See [this page](https://gitlab.com/gugal/beta/-/blob/main/latest_preview.md) for a link to the latest preview version.
