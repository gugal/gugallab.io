/*
 *     Calculator.kt
 *     Gugal
 *     Copyright (c) 2022-2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.porg.gugal.providers.calculator

import androidx.compose.runtime.snapshots.SnapshotStateList
import com.android.volley.toolbox.JsonObjectRequest
import com.porg.gugal.R
import com.porg.gugal.Result
import com.porg.gugal.providers.ProviderInfo
import com.porg.gugal.providers.SerpProvider

class Calculator: SerpProvider {
    companion object {
        val id: String = "366e0fd4acec4326b829216e2caf847f-calc"
        val providerInfo = ProviderInfo(R.string.serp_calc_title, R.string.serp_calc_desc, R.string.serp_calc_title, false)
    }
    /**
     * Information about this provider, like the name and description.
     */
    override val providerInfo: ProviderInfo?
        get() = Companion.providerInfo

    override fun search(query: String, setResults: ((List<Result>) -> (Unit)), setError: ((ErrorResponse) -> (Unit)): JsonObjectRequest? {
        val list: MutableList<Result> = mutableListOf()
        val answer = calculate(query)
        val result = Result(
            answer.toString(),
            "Tap to see more at Wolfram Alpha.",
            "https://www.wolframalpha.com/input?i2d=true&i=" + query.replace("+", "%2B"),
            "is $query"
        )
        list.add(result)
        setResults(list)
        return null
    }

    fun calculate(query: String): Int {
        // Get the operation
        val operation = getOperation(query)
        // Get the numbers
        val numbers = splitIntoNumbers(query)
        // Perform the operation and return the result
        return when (operation) {
            "*" -> numbers[0] * numbers[1]
            "/" -> numbers[0] / numbers[1]
            "+" -> numbers[0] + numbers[1]
            "-" -> numbers[0] - numbers[1]
            else -> 0
        }
    }

    private fun getOperation(query: String): String {
        return when {
            query.contains("*") -> "*"
            query.contains("/") -> "/"
            query.contains("+") -> "+"
            query.contains("-") -> "-"
            else -> "?"
        }
    }

    private fun splitIntoNumbers(query: String): Array<Int> {
        val splitStrings = query.split("*","/","+","-")
        val numArray: Array<Int> = Array(splitStrings.count()){ 0 }
        for ((index, str) in splitStrings.withIndex()) {
            numArray[index] = str.toInt()
        }
        return numArray
    }
}
