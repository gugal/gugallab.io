# Support for launcher search bars
Besides providing a search bar widget Gugal supports some popular search engine links, intended for integration with the search function of various stock and custom launchers:

- Google (`https://google.com`),
- Bing (`https://bing.com`),
- 2 pseudo domains:
    - `https://gugal.local`,
    - `https://gugal-search.local`

This page covers Gugal's support for these links.

## Set up
Before you can use this feature you might have to associate the links with Gugal.

> Note that the instructions below are written with stock Android (i.e. Android One phones, Google Pixel) in mind. The steps might be a bit different if you use another brand.

### Android <12
On Android versions before 12 no additional setup is necessary.

### Android 12 and above
On Android 12 and above you need to manually associate the search engine links with the app.

First, open the "App info" settings for Gugal. Then, go to "Open by default" and add the link for your preferred search engine. The selection doesn't matter as they all start a search with the engine that is used in the app.

> Some Google apps (like Maps) are associated with `google.com` by default, and Android won't let you associate Gugal with it because of that. The solution is to either:
> - go to their app info settings and disable "Open supported links" under "Open by default" (results in a less smooth experience with said apps),
> - uninstall those apps, or
> - use another domain to set Gugal up.

## Configuring launchers
For most launchers it should work automatically after setting it up. You might have to change the search engine in your launcher's settings.

However, some launchers require more setup. These are some of the ones that do and how to configure them. If you use one that requires another advanced process but isn't listed here feel free to contribute to this page.

### Lawnchair (alpha)
_Note that the version available in the Play Store is incompatible. This section is about the beta versions available [on GitHub](https://github.com/LawnchairLauncher/lawnchair/releases)._

Lawnchair 14 works with both Google and Bing. Lawnchair 12 doesn't work with `google.com`, so Gugal must be set up with `bing.com` instead.

1. If necessary, associate Gugal with `bing.com` or `google.com` (see the "Set up" section).
2. In Lawnchair's settings, go to Dock ⟶ Search Provider.
3. Select Bing or Google depending on which one was set up.
4. If you have the Bing/Google app installed, select Website.

When you tap the search field at the bottom of the home screen Gugal should start.

### Nova Launcher
Gugal can be set up using a `.local` link as Nova has a custom search provider feature.

1. If necessary, associate Gugal with `gugal.local` (see the "Set up" section).
2. In Nova Settings, go to Search ⟶ Search providers.
3. Tap Add.
4. Type in `gugal.local/?q=%s` as the URL. The name doesn't matter.

When you perform a search, you should see the new search engine. Tapping on it will send the search query to Gugal.

## Help! My launcher doesn't launch Gugal!
- Try redoing the steps in "Set up" again.
- If your launcher supports more than one search engine, set Gugal's launcher support up with another engine.
    - For example, if you associated Gugal with `google.com` and your launcher also supports Bing, try configuring it with `bing.com` instead.
- If you associated Gugal with `google.com`, try disabling or uninstalling the Google app and searching again.
    - If your launcher opens the Play Store or prompts you to enable the Google app, it's **unsupported**.
    - If it opens the browser, it might be supported. Please [make an issue](https://gitlab.com/narektor/gugal/-/issues/new) with the "App issue" template and specify the launcher name and version, along with other necessary information.
- Stock launchers are less likely to be supported than custom ones. Consider using a custom launcher.
- As a last resort, you can disable or remove the launcher's search widget (if possible) and use Gugal's widget.
