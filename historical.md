# Historical documentation

This section has documentation on deprecated features, features that were removed in newer versions of Gugal and 
other documentation that is no longer relevant.

Pages in this section will be removed in the future.

## Deprecated features
- [Simple builds](simple.md) _(supported from Gugal 0.7-0.8.x, to be removed in 0.9)_

## Outdated documentation
- [Updating SERP providers from Gugal 0.6 to 0.7](serp/search-6-to-7.md)