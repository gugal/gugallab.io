# Developer options

Gugal has a hidden developer options page, which contains settings that are useful for developing [SERP providers](serp/) and contributing to Gugal.

To access it, search `_gugal_devopts` using any search engine.

> Be careful! These settings are only intended for development and might lower security or break Gugal.

<img src="../res/devopt.png" width="300" alt="Image of Gugal's developer options, a list of settings.">

## The settings
Note that not all versions of Gugal have the same developer options. Some might be missing in older versions.

- **View encrypted preferences**: shows the data in Gugal's secure storage. Sensitive credentials are shown partially to avoid data leaks.
- **Restart setup**: restarts the app's setup wizard without having to clear data.
- **Development SERP providers**: shows a list of SERP providers intended for developing Gugal features.
- **Experiments**: shows a list of experimental features in this version. These features are in development.
