# Gugal

[![F-Droid](https://img.shields.io/f-droid/v/com.porg.gugal)](https://f-droid.org/en/packages/com.porg.gugal/) [![Latest Release](https://gitlab.com/narektor/gugal/-/badges/release.svg)](https://gitlab.com/narektor/gugal/-/releases) [![Weblate](https://hosted.weblate.org/widgets/gugal/-/svg-badge.svg)](https://hosted.weblate.org/engage/gugal/)

A clean, lightweight, FOSS web search app.

## Download

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png" alt="Get it on F-Droid" height="80">](https://f-droid.org/packages/com.porg.gugal/) [<img src="https://gitlab.com/narektor/gitlab-badges/-/raw/main/language/en/en-available-on.png" alt="Available on GitLab" height="80">](https://gitlab.com/narektor/gugal/-/releases)

Download from one _or_ the other. The versions are incompatible as F-Droid signs apps themselves.

## Set up

To use Gugal with Google Search you need a CSE (Programmable Search Engine) ID and API key. Instructions for obtaining both can be found in the app.

**To get these values you must have a Google account, therefore Google might still link searches to your account.**

To use Gugal with searx or SearXNG you need an instance **with the JSON search format enabled**. We recommend making your own instance as most public instances don't have it enabled.

## Contributing

### Translations
If you want to translate Gugal into your language or clean up an existing translation, check out our [Weblate](https://hosted.weblate.org/engage/gugal).

### SERP providers (beta)
See the [development guide](serp/dev/README.md).

### Code
Gugal is built with Jetpack Compose and Kotlin.
