# Only download Gugal from official sources!

Unofficial versions of Gugal can contain malware, spyware or adware. They might also be proprietary (no source code available), which violates the original app's license.

Also, Gugal saves credentials which are used to search, and an unofficial, malicious version of Gugal might steal those credentials. This might result in data being leaked and potentially cost you money as, for example, Google's Custom Search Engine API is only free for 100 queries per day.

## Which sources are official?

The only official sources are [the GitLab releases page](https://gitlab.com/narektor/gugal/-/releases) and [F-Droid](https://f-droid.org/en/packages/com.porg.gugal).
