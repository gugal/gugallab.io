# Backup and restore
This is a feature of Gugal to be introduced in version 0.9.

Backups can be locked with a password for more security.
> **Warning: the Gugal maintainers aren't responsible for API keys, passwords and other sensitive credentials getting leaked through unencrypted backups.**

Backups can be useful for storing a copy of Gugal's preferences in case a device gets damaged or lost. Another use case is transferring Gugal to a new device. The transfer process of some device manufacturers (like Samsung) also copies over Gugal's internal preference file, which is encrypted with a key that cannot be transferred, leading to errors where the app cannot be started unless its data is cleared or it is reinstalled. Backups provide a way to transfer preferences without relying on such keys.

> The rest of this document is meant for developers.

## Backup file format
Gugal backups are json files with the following fields:
- `gpbVersion` - the Gugal backup format version. This will be increased when a change that might break backwards compatibility is introduced.
- `gugalVersion` - the version of Gugal that created the backup.
- For locked backups:
    - `encryptedPreferences` - the preferences that were backed up, in encrypted form (see the section below).
- For backups that aren't locked:
    - `preferences` - the preferences that were backed up.

## Locked preference backups
Backups can be locked with a password. Locked backups have an `encryptedPreferences` field, with a string value in the form `{algorithm},{salt},{content}`.

`{content}` is a JSON object containing Gugal preferences, similar to the `preferences` object in regular backups, encrypted using the algorithm specified in `{algorithm}` (a Java specification). Encryption is performed with a key derived using PBKDF2 with HMAC SHA256 from the user's provided password with the following parameters:
- Iterations: 15000,
- Key length: 128 bit,
- Salt: `{salt}` in the original `encryptedPreferences` string + the magic string `FQUbCgI3UgUKBg1TDQMRT1M2FQ1ABhBTHBcFAB5/SB0BGQQcVFo`, converted into a UTF-8 array.
The resulting key is encoded as a hex string and used in encryption.

For more information see the 2 primary classes used during encryption: [AESCrypt.kt](https://gitlab.com/narektor/gugal/-/blob/8e2701c35/app/src/main/java/com/porg/gugal/common/AESCrypt.kt) and [KeyDerivation.kt](https://gitlab.com/narektor/gugal/-/blob/8e2701c35/app/src/main/java/com/porg/gugal/common/KeyDerivation.kt).
> The above files are permalinks to the versions of the files as of [8e2701c3](https://gitlab.com/narektor/gugal/-/commit/8e2701c3502f16dfca74e7e86b364dad50587aa4), the commit where format version 1 was introduced.

## Backup format version history
### Version 1
Initial release.