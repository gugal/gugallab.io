# Guidelines for SERP providers

_Last updated February 12, 2025._

![A wireframe of an Android phone. It shows a wireframe of Gugal's search results page, with extra blueprint-like lines around the first result.](../res/serp-guidelines/hero.png)
This page lists some guidelines for SERP providers, ensuring a good user experience. They must be followed for a SERP provider to be added in the app.

## Functionality
- A SERP provider must be able to perform basic searches, at the moment of the pull request's creation, without crashing or returning an error if the configuration is correct.
- It's preferred if the provider doesn't require signing into an account or any additional downloads.
- SERP providers must only use the SERP provider API to communicate with the app.
   - Explicit violations include:
      - Reading or editing `Global.sharedPreferences` directly instead of using the API methods for reading and writing sensitive data.
      - Starting any activity that is or isn't a part of Gugal.
         - If you want to ask users to change your SERP provider's settings, use error responses.
         - If you want to open any other screen, ask the user to open it themselves.
         - Forcing the app to navigate between tabs is disallowed too.
      - Opening any URL without the user's consent, even if it's one of the search results.
      - Changing any Gugal settings from the SERP provider, even if they're related to searching.
   - Modifying the SERP provider API itself, especially to access fields and/or methods that aren't a part of it, isn't allowed either.
      - You can request methods to be added to the SERP provider API by creating an issue on Gugal's repository.

## Design
These are about the SERP provider's `ConfigComposable`.

- A SERP provider's `ConfigComposable` may not use any UI elements (like buttons or input fields) from design systems other than [Material 3](https://m3.material.io).
   - If a search engine requires a component to look a certain way (e.g. a sign in button that must use the engine brand colors), the component is exempt from this requirement.
- The composable may not use re-implemented versions of components already implemented in Google's official Compose Material 3 library or Gugal's `com.porg.m3` package, unless limited by the existing implementation.
   - _SERP providers built for Gugal 0.9 (upcoming) may also use, but not re-implement, components from the [M3X](https://gitlab.com/cat-aspect/m3x) library._
- Avoid technical jargon and make the process of configuring your SERP provider as clear as possible to your target audience.
   - Names of actions users have to perform outside Gugal to configure search (e.g. going to an "API access" tab in a search engine's developer console) are exempt.
   - On a related note, Gugal doesn't use the term "SERP provider" to describe them to anyone who isn't a developer, so don't use it in your composable either.
      - You can use words like "search engine", "(brand name) support" or "(brand name) search" instead. For example:
         - "Enable the XYZ SERP provider" ⟶ "Select the XYZ search engine"
         - "The settings of the XYZ SERP provider" ⟶ "XYZ search settings"
         - "This SERP provider is in beta" ⟶ "Support for this search engine is in beta"

> ![Demo image](../res/serp-guidelines/design.png)
>
> 1 - a Material Design 2 input field was used.
>
> 2 - instead of the real Material 3 button component, a Material Design 2 button styled into a Material 3 one is used, as evidenced by the shadow.
>
> 3 - this button isn't a Material 3 component, but it's exempt as this design is required by the brand.
>
> 4 - this text uses technical terms like "activity" and "ANR" and should be rewritten.
>
> _Notice: This image uses a fictional brand. Any resemblance to a real company is coincidental._

## Advertising
- A SERP provider may not:
   - inject results or ads (even if labelled as such) not provided by the search engine into the search results,
   - add referral or affiliate codes to search result links or modify them to profit its creator in any other way,
   - change the ranking of search results to promote a specific result over others.
- If the search engine returns whether a result is sponsored to the SERP provider, the provider must display this information in Gugal.
   - For example, Google doesn't return this information for searches done with the CSE API, which is how the Google SERP provider gets search results, so this provider is exempt from this requirement. However, if it was implemented by scraping Google's web search page instead of using the CSE API, it would be required to display whether a result is sponsored, as Google displays this on its search results page.

> ![Demo image](../res/serp-guidelines/ads.png)
>
> This image shows a search result in Gugal done with a provider violating these rules (right) and the original search results page (left).
>
> 1 - the SERP provider has inserted an ad that doesn't exist in the original page.
>
> 2 - this is also an ad, but it's not considered a violation as it's also present in the original page.
>
> 3 - while the result wasn't inserted by the SERP provider, it was moved above a more relevant result.

## Security
- Credentials entered in a `ConfigComposable` (and, by extension, credentials sent by Gugal to the SERP provider when the latter is initialized) are treated as private, meaning that the provider may not transfer them to third parties (anyone but the search engine).
- _If a SERP provider is built for Gugal 0.9 (upcoming):_
   - _In case more credentials are required to set up a SERP provider, they may only be requested using the `requestCredentials` function of `ConfigComposable`._
   - _If possible, a credential should use one of the types specified in the `Credential` class's companion object._
   - _If defining a custom `Credential` type, the `hidden` attribute must be set to `true` if this credential is sensitive (i.e. has the potential to cause damage if leaked)._
- A provider may not change search result links to malicious or fake versions.
- Likewise, a provider may not edit search results to take advantage of vulnerabilities in sites or Gugal itself.

> ![Demo image](../res/serp-guidelines/phishing.png)
>
> The search engine shows Gugal's releases page on GitLab as the first result for a search for _"download gugal update"_. However, instead of the real releases page, the SERP provider provides a link to a website that looks like the official Gugal website and that has a link to download a malicious version of Gugal. All of the other result data, including the visible domain, is kept intact.
>
> The user clicks the result, expecting it to be the real Gugal download page, but instead gets taken to the fake site.

## Accessibility
- Make sure that everything in your `ConfigComposable` is labelled correctly for a screen reader.
   - You can try enabling TalkBack and getting it to read your composable.
