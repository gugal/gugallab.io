# SERP provider API changes

This document contains changes to the SERP provider API done in versions of Gugal.

## Changes in 0.8.x
### 0.8.2
- The `com.porg.gugal.Material3*` classes have been removed. Please use the alternatives in the `com.porg.m3` package. (https://gitlab.com/narektor/gugal/-/commit/642eb7d7ed8df8ee5561004fc369b1f8deab090a)
  - As this change is unexpected, please upgrade the Gugal source code to 0.8.1, migrate your config composables to `com.porg.m3`, then upgrade to 0.8.2.
### Preview 2
- The functions in the `com.porg.gugal.Material3*` classes have been moved to the `com.porg.m3` package, and now use a more Compose-style syntax. The `Material3*` variants are now deprecated and will be removed in 0.9. (https://gitlab.com/narektor/gugal/-/commit/e302e3c313665f1fce0bae8e4fbc4b2b5eae0558)
  - Most of the deprecated functions can be replaced with the new versions automatically - see Android Studio's tooltip when you hover over the function for details about migration.

### Preview 1
- The `Result` class has been moved to the `com.porg.gugal.data` package. (https://gitlab.com/narektor/gugal/-/commit/834878522d0ef53769660df4f5f536fa658dcc4d)
- Result metadata classes have been added. (https://gitlab.com/narektor/gugal/-/commit/4a1e8253c42122a64a4be4746c19b48858096cc5)
  - There are 2 classes:
    - `ResultMetadataProduct` for products,
    - `ResultMetadata` as a base class.
  - Currently these classes are unsupported.

## Changes in 0.7
### Preview 2
- `SerpProvider.search()` has been changed because of an app rewrite. For documentation see [Upgrading SERP providers from 0.6 to 0.7](search-6-to-7.md) _(historical - [more information](../historical.md))_. (https://gitlab.com/narektor/gugal/-/commit/423d1884543576908c3a2b6af8c211f01eb7b62b)

### Preview 1
- `ConfigComposable` now has a `context` argument (https://gitlab.com/narektor/gugal/-/commit/a8ef4b1fce372b606717b2de1a85358d658cc563).

## Changes in 0.6
### Preview 1
- Added support for search errors in async methods (like Volley's result handler). To make this possible an `ErrorResponse` class and some overrides for it were added, as well as a new `isError` argument for `search()`.

## Changes in 0.5
### Final release
- Added input validation for `ConfigComposable`. A SERP provider must enable the Next/Save button of the setup wizard manually by setting `ConfigComposable.enableNextButton` to true once the input is valid. (https://gitlab.com/narektor/gugal/-/commit/bffe3be2fd08aa5c846634b154d682d7c4fa7206)
- Fixed setup loops when the SERP provider doesn't require setup. (https://gitlab.com/narektor/gugal/-/commit/984d1b4e865476d047fb800bdeb2ab924e6e3844)
- `search()` functions that don't return a Volley JsonRequest no longer crash the app. (https://gitlab.com/narektor/gugal/-/commit/347aa0f2cd73dd61ca1c9614fac609cb83259fc3)

### Preview 2
- The `name`, `description` and `titleInSearchBox` fields in ProviderInfo are no longer strings, and are instead resource IDs pointing to the strings for this SERP provider's name, description and title in the search box. (https://gitlab.com/narektor/gugal/-/commit/541b83b2b608aed842ca01964b970c0886f5bb87)
