# Theory

This section describes how SERP providers work, before we get into writing the code itself.

## Q&A

### What is a SERP provider?

According to [SERP providers](serp/):

> SERP _(search engine results page)_ providers allow Gugal to search the web using different search engines, and can be used to search data on the device (e.g. contacts, files, messages).

### And from a technical standpoint?

From a technical standpoint SERP providers are Kotlin classes implementing the [`SerpProvider`](https://gitlab.com/narektor/gugal/-/blob/main/app/src/main/java/com/porg/gugal/providers/SerpProvider.kt) interface.

SERP providers identify themselves to the app internally using UUIDs and to the user using [`ProviderInfo`](https://gitlab.com/narektor/gugal/-/blob/main/app/src/main/java/com/porg/gugal/providers/ProviderInfo.kt) objects.

### Any other classes I need to know about?

SERP providers send results back to the app using [`Result`](https://gitlab.com/narektor/gugal/-/blob/main/app/src/main/java/com/porg/gugal/Result.kt) objects. If an error occured doing a web search, they can return [certain error responses](https://gitlab.com/narektor/gugal/-/tree/main/app/src/main/java/com/porg/gugal/providers/responses) instead.

### What about the license?

SERP providers are considered parts of Gugal, which is licensed under GPLv3.

Web search using proprietary search engines is allowed. Usage of proprietary libraries to perform search is discouraged to keep Gugal fully free (as in freedom) and open-source, and it's preferred if there are alternatives that can be called using free libraries or parts of Android (e.g. if a web search engine offering a proprietary search library also has a search API that is affordable, call the API using standard web requests). But, if you _must_ use a proprietary library, please also add a way to disable it when building the app (like adding a separate nonfree flavor).

## The lifecycle of a SERP provider

This part describes the interactions between Gugal and a SERP provider.

### When Gugal is started for the first time
1. Gugal shows its setup wizard, with the first page greeting the user.
2. After the user skips that page, Gugal shows a list of SERP providers defined in `Global.allSerpProviders`, getting data from the providers' `ProviderInfo`. The user should then select a SERP provider and tap Next.
3. Gugal shows the selected SERP provider's `ConfigComposable` in a setup page, letting the user configure any settings that might be required for searching. If the provider's `ProviderInfo.requiresSetup` is `false` this step is skipped.
4. Finally, Gugal shows the last setup page with a few tips.

### When Gugal is started subsequently
1. The app loads encrypted preferences and loads the picked SERP provider.
2. The app calls `getSensitiveCredentialNames()`. The provider should return an array of keys for its sensitive credentials (but not the credentials themselves!)
3. Gugal loads the credentials from encrypted preferences using the keys from the array.
4. The app calls `useSensitiveCredentials()` with a map where the keys are the ones from step 2 and the values are the corresponding credentials. Note that Gugal won't provide them during the current session anymore.

### When the user makes a search
1. Gugal calls `search()` with the query the user typed in and functions for providing the results (`setResults`) and an error response (`setError`).
2. The provider performs a search for the provided query and the credentials. If the provider implements web search using the Volley library it can return a web request, which will be performed by the app. Otherwise the provider should return null.
3. The app performs the web request if one was returned by the provider. If an error occurs during search, the provider should call the `setError` function with an appropriate [error response](https://gitlab.com/narektor/gugal/-/tree/main/app/src/main/java/com/porg/gugal/providers/responses).
4. When the search is finished the provider should add the results as `Result` objects to a mutable list and provide it to Gugal with the `setResults` function. The list will be shown by the app.

---

Go to [the next chapter](simple.md) to start developing your own SERP provider.