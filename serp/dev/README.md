# SERP provider development guide
So you want to add support for a new search engine? Then you've come to the right place.

This guide is still WIP, and up-to-date as of and developed for commit [f2a3e00e](https://gitlab.com/narektor/gugal/-/commit/f2a3e00ecdac15ac731fac6d9ac13064bc79b9cd). However, it should be fully functional in the latest stable version of Gugal, and will be kept up to date with the latest changes in Gugal's SERP provider API.

If you notice an issue, feel free to report it [on the site issue tracker](https://gitlab.com/gugal/gugal.gitlab.io/-/issues), but please add the "serp guide" label.

## About this guide
In this guide we will develop 2 SERP providers: a simpler one that performs calculations, and a more complex one calling an actual search API.

## Chapters
* [Theory](theory.md)
* [Your first SERP provider](simple.md)
* [A more advanced one](advanced.md) _(WIP)_