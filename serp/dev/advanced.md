# The more advanced SERP provider

This SERP provider will call the [Free Dictionary API](https://dictionaryapi.dev/) to get definitions of a word. It will also demonstrate how to make your own "Set up search" screen for configuring the SERP provider.
