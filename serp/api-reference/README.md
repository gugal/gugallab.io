# SERP provider API reference

The API reference contains documentation for all Gugal classes used in SERP providers, as well as the 
`com.porg.m3` library. It can be used to browse documentation on features not covered in the 
[SERP provider development guide](/serp/dev/).

Only the latest stable and preview version's API reference will be kept. However, for now, the API reference 
for commit [`a1d19859`](https://gitlab.com/narektor/gugal/-/commit/a1d19859b38808ede84048f054a50079bf77a2fa) 
is available as a preview; it will be replaced with that of Gugal 0.9 preview 1 once that version is out.

- [API reference preview](preview/)

## Generating the API reference

> This is currently only possible when building Gugal from the main branch.

The API reference is generated with Dokka. In the Gugal project, build the `docs` configuration instead of 
the `app` one. Generated docs can be found in `(project folder)/build/dokka`.

> If this configuration doesn't exist:
> 1. Go to the "Edit configurations..." menu.
> 2. Click on the plus icon, then **Gradle**.
> 3. Name it `docs` and enter `dokkaHtml dokkaHtml` under **Run**.