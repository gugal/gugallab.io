# What is a SERP provider?

SERP[^serp] providers allow Gugal to search the web using different search engines, and can be used to search data on the device (e.g. contacts, files, messages).

Currently Gugal ships with Google CSE and SearX providers, but more can be added. See the [development guide](creation.md) for developing new SERP providers, or the [list of API changes](api-changes.md) for updating existing ones.

SERP providers were introduced in Gugal 0.3, but became fully supported in 0.5.

[^serp]: search engine results page
