# Upgrading SERP providers from 0.6 to 0.7

> This page is considered historical documentation and might be removed in the future - more information [here](../historical.md).

Because the app has been rewritten to use view models, `SerpProvider.search()`'s parameters have been changed:
```kotlin
// pre 0.7:
fun search(
    query: String,
        resultList: SnapshotStateList<Result>,
            isError: MutableState<ErrorResponse?>
            ): JsonObjectRequest?
            // 0.7:
            fun search(
                query: String,
                    setResults: ((List<Result>) -> (Unit)),
                        setError: ((ErrorResponse) -> (Unit))
                        ): JsonObjectRequest?
                        ```

                        The main changes are that a list of results and mutable state for the error are no longer provided. Instead the result view model passes functions to set the results and error state, which you need to call instead of updating the 2 values.

                        Here is an example _(notice the second line and the `for` loop)_:
                        ```kotlin
                        // pre 0.7:
                        if (searchError)
                            isError.value = ErrorResponse("Something bad happened", "403")
                            else
                                for (res in results) {
                                        resultList.add(
                                                    Result(
                                                                    res.getString("title"), item.getString("description"),
                                                                                    res.getString("link"), item.getString("domain")
                                                                                                )
                                                                                                        )
                                                                                                            }

                                                                                                            // 0.7:
                                                                                                            if (searchError)
                                                                                                                setError(ErrorResponse("Something bad happened", "403"))
                                                                                                                else
                                                                                                                    // now you should create the result list
                                                                                                                        val list: MutableList<Result> = mutableListOf()
                                                                                                                            for (res in results) {
                                                                                                                                    list.add(
                                                                                                                                                Result(
                                                                                                                                                                res.getString("title"), item.getString("description"),
                                                                                                                                                                                res.getString("link"), item.getString("domain")
                                                                                                                                                                                            )
                                                                                                                                                                                                    )
                                                                                                                                                                                                        }
                                                                                                                                                                                                            // and pass it to the app
                                                                                                                                                                                                                setResults(list)
                                                                                                                                                                                                                ```