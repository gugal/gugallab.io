# Test versions

Test versions of Gugal are built to test a fix for a specific issue or to test changes made by other developers using merge requests. They are less stable than regular or [preview](/preview.html) versions.

Test versions can be installed as updates to direct (GitLab) versions. Unlike preview versions, it's possible to return from a test version to the version it's based on, which is usually stable.

> For example, it's possible to go from `0.8.2-if-35-searx-allow-basic-auth-db6e4aa` to Gugal 0.8.2 or later, but not to any earlier version.

## Version number structure

The version numbers follow this pattern: `(base gugal version)-if-(branch)-(commit)`. Most of this is technical information that is useful for developers working on an issue.

Test versions meant to test changes made in merge requests instead follow this pattern: `(base gugal version)-pr-(branch)-(commit)`.

> For example, in the case of `0.8.2-if-35-searx-allow-basic-auth-db6e4aa`:
> - this version is based on Gugal 0.8.2,
> - the code for this version is on branch [`35-searx-allow-basic-auth`](https://gitlab.com/narektor/gugal/-/tree/35-searx-allow-basic-auth),
> - the code for this version is in commit [`db6e4aa`](https://gitlab.com/narektor/gugal/-/commit/db6e4aa0728f078baca5c8acfc74bdcec4a75e3b)

## Building a test version

This section is meant for developers that want to make a test version.

> Warning: if you want to build a test version based on Gugal 0.8.2 or an earlier version, pick commit [a93b4a5](https://gitlab.com/narektor/gugal/-/commit/a93b4a5d445af18a6b6cbdec37692ffcf05f1312). This commit adds test version support, but removes simple versions.

Make sure that you have git installed. Then, when building, use the flavor `issueTesting` (for issue fixes) or `prTesting` (for merge requests).